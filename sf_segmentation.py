# Segments Loader

import boto3
from botocore.exceptions import ClientError
import os
import pathlib
import logging

import pysftp
import paramiko
from base64 import decodebytes

import pandas as pd
from sqlalchemy.engine import url as sa_url
from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import text
import logging
from datetime import datetime


class Redshift:
    """
    Class to manage the connection to redshift

    Parameters
    ----------

    host:str

    port:int

    dbname:str

    user:str

    password:str

    Properties
    ----------

    connection_url:str

    engine:sqlalchemy.Engine

    """

    def __init__(self, host, port, dbname, user, password, echo=False):

        self.logger = logging.getLogger(self.__class__.__name__)

        self.connection_url = sa_url.URL(
            drivername='postgresql+psycopg2',
            username=user,
            password=password,
            host=host,
            port=port,
            database=dbname
        )

        self.logger.debug(f'Connection URL: {self.connection_url}')

        try:
            self.engine = create_engine(self.connection_url, echo=echo)
        except Exception as e:
            self.logger.error("Error Connecting to redshift")
            self.logger.exception(e)
            raise e

    def query(self, query, return_df=False, chunksize=None):
        """
        Run a query in the connected database

        Parameters
        ----------
        query:str
        The SQL code to run in the database

        return_df:bool
        Do you want to return the results as a dataframe?

        Returns
        -------
        pd.DataFrame
        The results of your query
        """
        _s = datetime.now()

        # if you want to return the method to return a dataframe then we use a different method
        if return_df:
            output = self._df_query(query, chunksize)

        # If they do not want the results we can just execute the query on the redshift side
        else:
            self._raw_query(query)
            output = None

        _e = datetime.now()
        self.logger.info(f"Query took {str(_e - _s)}")

        return output

    def _df_query(self, query: str, chunksize: int = None) -> pd.DataFrame:
        """Run a query against redshift and return a dataframe

        Args:
            query (str): The query
            chunksize (int): To chunk the output enter a value. Defaults to None.

        Raises:
            e: Raises an error if the query fails

        Returns:
            pd.DataFrame: The output Dataframe
        """
        try:
            output = pd.read_sql(sql=query, con=self.engine,
                                 chunksize=chunksize)
            self.logger.info(f"Output df shape: {output.shape}")
        except Exception as e:
            self.logger.error("Error running query to get dataframe")
            self.logger.exception(e)
            raise e

        return output

    def _raw_query(self, query: str) -> None:
        """Execute a query in redshift and don't extract any results

        Args:
            query (str): The query

        Raises:
            e: Any error raised during execution
        """
        statement = text(query)

        try:
            with self.engine.connect() as con:
                con.execute(statement)
        except Exception as e:
            self.logger.error("Error running query")
            self.logger.exception(e)
            raise e

    def close(self) -> None:
        """
        Close the connection to redshift
        """
        self.engine.dispose()

    def table_exists(self, table_name: str, schema: str = None) -> bool:
        """
        Check if a table exists in the data warehouse
        """
        return self.engine.dialect.has_table(self.engine, table_name, schema)

    def upload_dataframe(
        self,
        df: pd.DataFrame,
        table_name: str,
        schema: str = None,
        if_exists: str = 'append',
        index: str = False,
        index_label: str = None,
        chunksize: int = 10000,
        dtype: dict = None,
        method: str = 'multi'
    ) -> None:
        """
        Upload a dataframe to the redshift data warehouse. 

        Used the `pd.DataFrame.to_sql` method but changed some defaults

        Parameters
        ----------
        Look at the parameters for `pd.DataFrame.to_sql`
        """

        self.logger.info(f"Uploading dataframe of shape: {df.shape}")
        _s = datetime.now()

        df.to_sql(
            table_name,
            con=self.engine,
            schema=schema,
            if_exists=if_exists,
            index=index,
            index_label=index_label,
            chunksize=chunksize,
            dtype=dtype,
            method=method
        )

        _e = datetime.now()
        self.logger.info(f"upload_table took {str(_e - _s)}s")

    def drop_table(self, table_name: str, schema: str = None) -> None:
        """
        Drop a table in redshift
        """

        _s = datetime.now()

        self.logger.info(f"Drop Table {schema}.{table_name}")
        self.query(f'drop table if exists {schema}.{table_name};')

        _e = datetime.now()
        self.logger.info(f"drop_table took {str(_e - _s)}s")

    def get_count(self, table_name: str, schema: str,  **kwargs) -> int:
        """
        Get the number of rows in a given table given conditions
        """

        where_conditions = '\n'.join(
            [f"and {k} = {v}" for k, v in kwargs.items()])

        sql = f"""
        select count(*) from {schema}.{table_name}
        where 1=1
        {where_conditions}
        """

        count = self.query(sql, return_df=True)

        return count.values[0][0]

    def delete_from(self, table_name: str, schema: str, **kwargs) -> None:
        """
        Delete from a table given conditions
        """

        where_conditions = '\n'.join(
            [f"and {k} = {v}" for k, v in kwargs.items()])

        sql = f"""
        delete from {schema}.{table_name}
        where 1=1
        {where_conditions}
        """

        self.query(sql)

def df2dmp(df: pd.DataFrame, key: str):
    """
    Convert a pandas DataFrame into the required format for the DMP

    Parameters
    ----------

    df: pd.DataFrame
    The dataframe in to format

    key: str
    The key for the DMP (Normally a customer ID)

    Returns
    -------

    ps.Series: A string for each row of the dataframe with the associated format
    """

    output = df[key] + '^'

    for c in df.columns:
        if c != key:
            output += c + ':' + df[c] + ';'

    # remove the last semicolon
    output = output.map(lambda x: x[:-1])

    return output


# Upload to SalesForce


def upload_file(file_name, bucket, session=None, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param session: The boto3 session. If not specified uses the local config.
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    logging.info(
        f"Uploading File: {file_name} to {os.path.join(bucket, object_name)}")

    # Upload the file
    if session is None:
        s3_client = boto3.client('s3')
    else:
        s3_client = session.client('s3')

    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.info("Error moving file")
        logging.error(e)
        return False

    logging.info("Success")
    return True


def dmp_date_str(hour_cutoff=11):

    # if we are after the cutoff we are now on today
    if datetime.now().hour >= hour_cutoff:
        dt = datetime.now()
    else:
        # otherwise we are dealing with yesterday
        dt = datetime.now() - timedelta(days=1)

    # format the date
    dt_str = f'{dt.year}-{dt.month:02d}-{dt.day:02d}'

    return dt_str


def copy_to_sftp(local_path, sftp_path, sftp_credentials):

    logging.info(f'Connect to host: {sftp_credentials["host"]}')

    # add the ssh key to the options
    keydata = b"AAAAB3NzaC1yc2EAAAABEQAAAQEA2cqHc2pdlOQeADtImXS/T0B5xEQm8tR/iWBI45ytfZbMBBDrbzGCXV94Yu/Vqtzrbz4YoxBUL9QGHTLVYzkNVbRseQaENGmUpE6LbAGkWou4pFxuqNAneRXFFQkcSX8HAmqufmaUZnKr2wSCaxUyIFPT4DEQ/xtLE5jrFbh1szxASK2ByGADOEaX4IZkfOZ26IOsFKGRbBYjotjF7KalsXL56316o7VQlbXgPGLFfX7ie8Ig7b0myG/YuHxS7E7sTxQTDjhIv7fl84XkhBpLvGhDi/ZiYTjS+Fiv8ejiGo45SPiau6cg74f650CQX+me9zQvYp/Vb8iiBnuaT/K40Q=="
    key = paramiko.RSAKey(data=decodebytes(keydata))
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys.add(sftp_credentials['host'], 'ssh-rsa', key)

    logging.info(f'Moving File: {local_path} to SFTP: {sftp_path}')

    try:
        with pysftp.Connection(cnopts=cnopts, **sftp_credentials) as sftp:
            sftp.put(local_path, sftp_path)
    except Exception as e:
        logging.error("Error moving file")
        logging.exception(e)
        return False

    logging.info("Success")
    return True


sasreader_credentials = {
    'host': 'us-east-cprod-mdap-au-redshift1.ciuu2sg591tg.us-east-1.redshift.amazonaws.com',
    'dbname': 'dev',
    'port': '5439',
    'user': 'sasreader',
    'password': 'Lxygw7f1'
}

s3_credentials = {
    'aws_access_key_id': 'AKIASWJ7WLN7VACRII7R',
    'aws_secret_access_key': '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'
}

sftp_credentials = {
    'host': 'mcbvxp9t9b2h6z7mj46r434qcjj0.ftp.marketingcloudops.com',
    'port': 22,
    'username': '6232116_SAS',
    'password': 'xr5fi4m3dq7!'
}


if __name__ == "__main__":

    # build the logger
    logging.basicConfig(
        filename=os.path.join(
            pathlib.Path(__file__).parent.absolute(),
            'sf_segmentation.log'
        ),
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO
    )

    # create a redshift instance
    rs_sasreader = Redshift(**sasreader_credentials)

    logging.info('Get Customer Segments from Redshift')

    # find the current segments from the view in redshift
    customer_segments = rs_sasreader.query(
        '''
        select 
            mobile_customer_id as ecp_id
            ,rf_segment
            ,food_segment
            ,monetary_segment
            ,offer_segment
        from annalect.customer_segments
        ''',
        return_df=True
    )

    logging.info('Convert DataFrame to DMP format')

    # convert the view into the correct format for the DMP
    # FYI it is a terrible format
    output = df2dmp(customer_segments, 'ecp_id')

    # get the appropriate date str for now
    date_str = dmp_date_str(11)

    file_name = f'outputs/segments_dmp.csv.gz'

    logging.info(f'Save output to {file_name}')
    try:
        # save the outputs into the
        pd.DataFrame(output).to_csv(file_name, columns=None,
                                    header=False, index=False, compression='gzip')
    except Exception as e:
        logging.error(f'Error saving DMP outputs to {file_name}')
        logging.exception(e)

    # bucket name and where to dump stuff
    sf_bucket = 'krux-partners'
    dir_name = 'client-mcdonalds-au/uploads/propensity'

    # set up the AWS session
    session = boto3.Session(
        **s3_credentials
    )

    # move file to the S3 bucket
    result = upload_file(
        file_name,
        sf_bucket,
        session,
        f'{dir_name}/{date_str}/segments_dmp.csv.gz'
    )

    # save local file version for SFTP
    mc_file_name = f'ann_segmentation_{date_str}.csv.gz'
    logging.info(f"Save normal segmentation format to {mc_file_name}")
    try:
        customer_segments.to_csv(os.path.join(
            'outputs', mc_file_name), compression='gzip', index=False)

        # move it to the SFTP
        copy_to_sftp(os.path.join('outputs', mc_file_name),
                    f'/GMA Offers Segmentation/{mc_file_name}', sftp_credentials)    
    except Exception as e:
        logging.error(f'Error saving normal segmentation to {file_name}')
        logging.exception(e)

    # find all files in the output dir
    files = os.listdir('outputs')
    for f in files:

        # find all previous files
        if f < mc_file_name:
            
            # delete the file so we don't take up too much local space
            os.remove(os.path.join('outputs', f))

    logging.info("Complete")
